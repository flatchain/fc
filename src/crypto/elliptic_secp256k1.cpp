#include <fc/crypto/elliptic.hpp>

#include <fc/crypto/base58.hpp>
#include <fc/crypto/hmac.hpp>
#include <fc/crypto/openssl.hpp>
#include <fc/crypto/sha512.hpp>
#include <fc/crypto/sha256.hpp>

#include <fc/fwd_impl.hpp>
#include <fc/exception/exception.hpp>
#include <fc/log/logger.hpp>

#include <secp256k1.h>
#include <secp256k1_commitment.h>
#include <secp256k1_generator.h>
#include <secp256k1_elgamal.h>
#include <secp256k1_bulletproofs.h>

#if _WIN32
# include <malloc.h>
#elif defined(__FreeBSD__)
# include <stdlib.h>
#else
# include <alloca.h>
#endif

#include "_elliptic_impl_priv.hpp"
#include <fc/crypto/cipher.hpp>
#include "_digest_common.hpp"

namespace fc { namespace ecc {
    namespace detail
    {
        const secp256k1_context* _get_context() {
            static secp256k1_context* ctx = secp256k1_context_create(SECP256K1_CONTEXT_VERIFY | SECP256K1_CONTEXT_SIGN);
            return ctx;
        }

        secp256k1_scratch_space* _get_scratch_space() {
            static secp256k1_scratch_space *scratch = NULL;
            static int inited = 0;
            if (!inited) {
                scratch = secp256k1_scratch_space_create(_get_context(), 256 * (1 << 20));
            }
            return scratch;
        }

        secp256k1_bulletproof_generators* _get_bp_generators() {
            static secp256k1_bulletproof_generators *generators = NULL;
            static int inited = 0;
            if (!inited) {
                generators = secp256k1_bulletproof_generators_create(_get_context(), &secp256k1_generator_const_g, 128);
            }
            return generators;
        }


        void _init_lib() {
            static const secp256k1_context* ctx = _get_context();
            static int init_o = init_openssl();
            static secp256k1_scratch_space* space = _get_scratch_space();
            static secp256k1_bulletproof_generators* generators = _get_bp_generators();
            (void)ctx;
            (void)init_o;
            (void)space;
            (void)generators;
        }

        secp256k1_pubkey _get_pubkey(const secp256k1_context* ctx, const public_key_data& pub) {
            secp256k1_pubkey pubkey;
            FC_ASSERT( secp256k1_ec_pubkey_parse(ctx, &pubkey, (unsigned char*)pub.begin(), pub.size() ) );
            return pubkey;
        }

        class public_key_impl
        {
            public:
                public_key_impl() BOOST_NOEXCEPT
                {
                    _init_lib();
                }

                public_key_impl( const public_key_impl& cpy ) BOOST_NOEXCEPT
                    : _key( cpy._key )
                {
                    _init_lib();
                }

                public_key_data _key;
        };

        typedef fc::array<char,37> chr37;
        chr37 _derive_message( const public_key_data& key, int i );
        fc::sha256 _left( const fc::sha512& v );
        fc::sha256 _right( const fc::sha512& v );
        const ec_group& get_curve();
        const private_key_secret& get_curve_order();
        const private_key_secret& get_half_curve_order();

        // For confidential usage
        // ciphertext
        class ciphertext_impl
        {
        public:
            ciphertext_impl() BOOST_NOEXCEPT
            {
                _init_lib();    // although no need to use openssl
            }

            ciphertext_impl(const ciphertext_impl& cpy) BOOST_NOEXCEPT
                : _cipher(cpy._cipher)
            {
                _init_lib();
            }

            ciphertext_data _cipher;
        };

        // rangeproof
        class rangeproof_impl
        {
        public:
            rangeproof_impl() BOOST_NOEXCEPT
            {
                _init_lib();
            }

            rangeproof_impl(const rangeproof_impl& cpy) BOOST_NOEXCEPT
                : _proof(cpy._proof)
            {
                _init_lib();
            }

            range_proof_type _proof;
        };

        // pederson commit
        class pederson_commit_impl
        {
        public:
            pederson_commit_impl() BOOST_NOEXCEPT
            {
                _init_lib();
            }

            pederson_commit_impl(const pederson_commit_impl& cpy) BOOST_NOEXCEPT
                : _commit(cpy._commit)
            {
                _init_lib();
            }

            commitment_type _commit;
        };

        // equal_prove
        class equal_prove_impl
        {
        public:
            equal_prove_impl() BOOST_NOEXCEPT
            {
                _init_lib();
            }

            equal_prove_impl(const equal_prove_impl& cpy) BOOST_NOEXCEPT
                : _prove(cpy._prove)
            {
                _init_lib();
            }

            equal_prove_data _prove;
        };

        // valid_prove
        class valid_prove_impl
        {
        public:
            valid_prove_impl() BOOST_NOEXCEPT
            {
                _init_lib();
            }

            valid_prove_impl(const valid_prove_impl& cpy) BOOST_NOEXCEPT
                : _prove(cpy._prove)
            {
                _init_lib();
            }

            valid_prove_data _prove;
        };

    }

    static const public_key_data empty_pub;
    static const private_key_secret empty_priv;
    static const random_type empty_random;
    static const ciphertext_data empty_cipher;
    static const equal_prove_data empty_eq;
    static const size_t SCRATCH_SPACE_SIZE = 256 * (1 << 20);
    static const size_t MAX_GENERATORS = 256;

    fc::sha512 private_key::get_shared_secret( const public_key& other )const
    {
      FC_ASSERT( my->_key != empty_priv );
      FC_ASSERT( other.my->_key != empty_pub );
      public_key_data pub(other.my->_key);
      secp256k1_pubkey pubkey;
      size_t klen = 33;
      pubkey = detail::_get_pubkey(detail::_get_context(), pub);
      FC_ASSERT( secp256k1_ec_pubkey_tweak_mul( detail::_get_context(), &pubkey, (unsigned char*) my->_key.data() ) );
      FC_ASSERT( secp256k1_ec_pubkey_serialize(detail::_get_context(), (unsigned char*) pub.begin(), &klen, &pubkey, SECP256K1_EC_COMPRESSED) );
      return fc::sha512::hash( pub.begin() + 1, pub.size() - 1 );
    }


    public_key::public_key() {}

    public_key::public_key( const public_key &pk ) : my( pk.my ) {}

    public_key::public_key( public_key &&pk ) : my( std::move( pk.my ) ) {}

    public_key::~public_key() {}

    public_key& public_key::operator=( const public_key& pk )
    {
        my = pk.my;
        return *this;
    }

    public_key& public_key::operator=( public_key&& pk )
    {
        my = pk.my;
        return *this;
    }

    bool public_key::valid()const
    {
      return my->_key != empty_pub;
    }

    public_key public_key::add( const fc::sha256& digest )const
    {
        FC_ASSERT( my->_key != empty_pub );
        public_key_data new_key;
        size_t klen = 33;
        memcpy( new_key.begin(), my->_key.begin(), new_key.size() );
        secp256k1_pubkey pubkey = detail::_get_pubkey(detail::_get_context(), new_key);
        FC_ASSERT( secp256k1_ec_pubkey_tweak_add( detail::_get_context(), &pubkey, (unsigned char*) digest.data() ) );
        FC_ASSERT( secp256k1_ec_pubkey_serialize(detail::_get_context(), (unsigned char*) new_key.begin(), &klen, &pubkey, SECP256K1_EC_COMPRESSED) );
        return public_key( new_key );
    }

    std::string public_key::to_base58() const
    {
        FC_ASSERT( my->_key != empty_pub );
        return to_base58( my->_key );
    }

    public_key_data public_key::serialize()const
    {
        FC_ASSERT( my->_key != empty_pub );
        return my->_key;
    }

    public_key_point_data public_key::serialize_ecc_point()const
    {
        FC_ASSERT( my->_key != empty_pub );
        public_key_point_data dat;
        secp256k1_pubkey pubkey;
        size_t pk_len = my->_key.size();
        memcpy( dat.begin(), my->_key.begin(), pk_len );
        FC_ASSERT( secp256k1_ec_pubkey_parse(detail::_get_context(), &pubkey, (unsigned char*)dat.begin(), pk_len ) );
        FC_ASSERT( secp256k1_ec_pubkey_serialize(detail::_get_context(), (unsigned char*) dat.begin(), &pk_len, &pubkey, 0) );
        FC_ASSERT( pk_len == dat.size() );
        return dat;
    }

    public_key::public_key( const public_key_point_data& dat )
    {
        const char* front = &dat.data[0];
        if( *front == 0 ){}
        else
        {
            EC_KEY *key = EC_KEY_new_by_curve_name( NID_secp256k1 );
            key = o2i_ECPublicKey( &key, (const unsigned char**)&front, sizeof(dat) );
            FC_ASSERT( key );
            EC_KEY_set_conv_form( key, POINT_CONVERSION_COMPRESSED );
            unsigned char* buffer = (unsigned char*) my->_key.begin();
            i2o_ECPublicKey( key, &buffer ); // FIXME: questionable memory handling
            EC_KEY_free( key );
        }
    }

    public_key::public_key( const public_key_data& dat )
    {
        my->_key = dat;
    }

    public_key::public_key( const compact_signature& c, const fc::sha256& digest, bool check_canonical )
    {
        int nV = c.data[0];
        if (nV<27 || nV>=35)
            FC_THROW_EXCEPTION( exception, "unable to reconstruct public key from signature" );

        if( check_canonical )
        {
            FC_ASSERT( is_canonical( c ), "signature is not canonical" );
        }

        unsigned int pk_len;
        FC_ASSERT( secp256k1_ecdsa_recover_compact( detail::_get_context(), (unsigned char*) digest.data(), (unsigned char*) c.begin() + 1, (unsigned char*) my->_key.begin(), (int*) &pk_len, 1, (*c.begin() - 27) & 3 ) );
        FC_ASSERT( pk_len == my->_key.size() );
    }

    // Confidential implementation
    ciphertext::ciphertext(const ciphertext_data& data)
    {
        my->_cipher = data;
    }

    ciphertext_data ciphertext::serialize() const
    {
        FC_ASSERT( my->_cipher != empty_cipher );
        return my->_cipher;
    }

    bool ciphertext::valid() const
    {
        return my->_cipher != empty_cipher;
    }


    ciphertext ciphertext::generate(const public_key& pubkey, const random_type& r, unsigned int v)
    {
        FC_ASSERT(pubkey.valid());

        if (r == empty_random)
            FC_THROW_EXCEPTION(exception, "invalid random to generate ciphertext");

        ciphertext_data cd;
        secp256k1_pubkey pb = detail::_get_pubkey(detail::_get_context(), pubkey.serialize());
        FC_ASSERT( secp256k1_elgamal_encrypt( detail::_get_context(), (unsigned char*)cd.begin(), &pb, (unsigned char *)r.begin(), v) );

        return ciphertext(cd);
    }

    rangeproof::rangeproof(const range_proof_type& data)
    {
        my->_proof = data;
    }

    rangeproof rangeproof::generate(const random_type& r, uint64_t v)
    {
        if (r == empty_random)
            FC_THROW_EXCEPTION(exception, "invalid random to generate rangeproof");

        range_proof_type proof;
        secp256k1_scratch_space *scratch = NULL;
        secp256k1_bulletproof_generators *generators = NULL;
        unsigned  char blind[32] = { 0 };
        memcpy(blind, r.begin(), r.size());
        const unsigned  char * bld = &blind[0];
        size_t plen = 0;
        do {
            scratch = secp256k1_scratch_space_create(detail::_get_context(), SCRATCH_SPACE_SIZE);
            if (!scratch) break;

            generators = secp256k1_bulletproof_generators_create(detail::_get_context(), &secp256k1_generator_const_g, MAX_GENERATORS);
            if (!generators) break;

            FC_ASSERT( secp256k1_bulletproof_rangeproof_prove(detail::_get_context(), scratch, generators, (unsigned char*)proof.data(), (size_t*)&plen, NULL, NULL, NULL, &v, NULL, &bld, NULL, 1, &secp256k1_generator_const_h, 64, r.begin(), NULL, NULL, 0, NULL) );
        } while (0);

        if (generators) secp256k1_bulletproof_generators_destroy(detail::_get_context(), generators);
        if (scratch) secp256k1_scratch_space_destroy(scratch);

        return rangeproof(proof);
    }

    pederson_commit::pederson_commit(const commitment_type& data)
    {
        my->_commit = data;
    }

    pederson_commit pederson_commit::generate(const random_type& r, uint64_t v)
    {
        if (r == empty_random)
            FC_THROW_EXCEPTION(exception, "invalid random to generate pederson commit");

        secp256k1_pedersen_commitment commit;
        commitment_type cmt_data;
        FC_ASSERT( secp256k1_pedersen_commit(detail::_get_context(), &commit, r.begin(), v, &secp256k1_generator_const_h, &secp256k1_generator_const_g) );
        FC_ASSERT( secp256k1_pedersen_commitment_serialize(detail::_get_context(), (unsigned char*)cmt_data.begin(), &commit) );

        return pederson_commit(cmt_data);
    }

    equal_prove::equal_prove(const equal_prove_data& data)
    {
        my->_prove = data;
    }

    bool equal_prove::valid() const
    {
        return my->_prove != empty_eq;
    }

    equal_prove_data equal_prove::serialize()const
    {
        FC_ASSERT( my->_prove != empty_eq );
        return my->_prove;
    }

    equal_prove equal_prove::generate(const ciphertext& c1, const ciphertext& c2, const private_key& prikey, const random_type& r)
    {
        if(!c1.valid() || c2.valid())
            FC_THROW_EXCEPTION(exception, "invalid ciphertext to generate equal prove");

        if (r == empty_random)
            FC_THROW_EXCEPTION(exception, "invalid random to generate equal prove");

        FC_ASSERT( prikey.get_secret() != empty_priv );

        equal_prove_data ep;
        FC_ASSERT( secp256k1_elgamal_equal_prove(detail::_get_context(), (unsigned char*)ep.begin(), c1.serialize().begin(), c2.serialize().begin(),
                                      reinterpret_cast<const unsigned char *>(prikey.get_secret().data()),
                                      r.begin()) );
        return equal_prove(ep);
    }

    bool equal_prove::verify(const equal_prove& prove, const ciphertext& c1, const ciphertext& c2, const public_key& pubkey)
    {
        if (!prove.valid()) return false;
        if (!c1.valid() || !c2.valid()) return false;
        if (!pubkey.valid()) return false;

        secp256k1_pubkey pk = detail::_get_pubkey(detail::_get_context(), pubkey.serialize());
        return !!secp256k1_elgamal_equal_verify(detail::_get_context(), prove.serialize().begin(),
                c1.serialize().begin(), c2.serialize().begin(), &pk);

    }

    valid_prove::valid_prove(const valid_prove_data& data)
    {
        my->_prove = data;
    }

    valid_prove valid_prove::generate(const public_key& pubkey, const random_type& r1, const random_type& r2, const random_type& r,
                             unsigned int v)
    {
        FC_ASSERT(pubkey.valid());

        if (r1 == empty_random || r2 == empty_random || r == empty_random)
            FC_THROW_EXCEPTION(exception, "invalid random to generate valid prove");

        valid_prove_data vp;
        secp256k1_pubkey pb;
        pb = detail::_get_pubkey(detail::_get_context(), pubkey.serialize());
        FC_ASSERT( secp256k1_elgamal_valid_prove(detail::_get_context(), vp.begin(), &pb, r1.begin(), r2.begin(), r.begin(), v) );
        return valid_prove(vp);
    }

}

cipher operator << ( const cipher& h1, uint32_t i ) {
    cipher result;
    fc::detail::shift_l(h1.data(), result.d, result.data_size(), i );
    return result;
}

cipher operator >> ( const cipher& h1, uint32_t i ) {
    cipher result;
    fc::detail::shift_r(h1.data(), result.d, result.data_size(), i );
    return result;
}

void to_variant( const cipher& bi, variant& v )
{
    v = std::vector<char>( (const char*)&bi, ((const char*)&bi) + sizeof(bi) );
}

void from_variant( const variant& v, cipher& bi )
{
    std::vector<char> ve = v.as< std::vector<char> >();
    if( ve.size() )
    {
        memcpy(&bi, ve.data(), fc::min<size_t>(ve.size(),sizeof(bi)) );
    }
    else
        memset( &bi, char(0), sizeof(bi) );
}

int get_hex(char c)
{
    if(c >= '0' && c<= '9')
        return c - '0';
    else if(c >= 'a' && c<= 'f')
        return c - 'a' + 10;
    else if(c >= 'A' && c<= 'F')
        return c - 'A' + 10;
    else
        return -1;//error
}

unsigned char* hex_to_bin(const char* hexStr, size_t hexsz, unsigned char * result, size_t binsz)
{
    if (binsz * 2 < hexsz) return NULL;

    for(int i = 0; i < hexsz; i +=2) {
        result[i/2] = (get_hex(hexStr[i])*16 + get_hex(hexStr[i+1]));
    }
    
    return result;
}

char * bin_to_hex(const unsigned char *bin, size_t binsz, char *result, size_t hexsz)
{
    unsigned char hex_str[]= "0123456789abcdef";
    unsigned int i;
    
    if (hexsz < binsz * 2) return NULL;

    if (!binsz) return NULL;
    
    for (i = 0; i < binsz; i++) {
        result[i * 2 + 0] = hex_str[(bin[i] >> 4) & 0x0F];
        result[i * 2 + 1] = hex_str[(bin[i]     ) & 0x0F];
    }
    return result;
}

bool is_ecdsa_signature_legal(const char* eq_para, size_t eq_len, const char* from_pub_para, size_t fp_len, const char* to_pub_para, size_t tp_len, const char* signature, size_t sig_len) {
    if (eq_len < 576 || fp_len < 1990 || tp_len < 1990 || sig_len < 128) return false;
    if (!eq_para || !from_pub_para || !to_pub_para || !signature) return false;
    if (strlen(eq_para) < eq_len 
        || strlen(from_pub_para) < fp_len
        || strlen(to_pub_para) < tp_len
        || strlen(signature) < sig_len) {
            return false;
    }

    secp256k1_ecdsa_signature sig;
	memset(&sig.data, 0, sizeof(sig.data));
	hex_to_bin(signature, 128, sig.data, 64);

	unsigned char eq_para_sz[288] = { 0 };
	hex_to_bin(eq_para, 576, eq_para_sz, 288);

	unsigned char from_pub_para_sz[995] = { 0 };
	hex_to_bin(from_pub_para, 1990, from_pub_para_sz, 995);

	unsigned char to_pub_para_sz[995] = { 0 };
	hex_to_bin(to_pub_para, 1990, to_pub_para_sz, 995);

	secp256k1_pubkey pubkey;
	memset(&pubkey, 0, sizeof(pubkey));
	if (!secp256k1_elgamal_pubkey(ecc::detail::_get_context(), &pubkey, &eq_para_sz[64], &from_pub_para_sz[64], &to_pub_para_sz[64])) return false;

    sha256::encoder encoder;
    encoder.write((const char *)eq_para_sz, 288);
    encoder.write((const char *)from_pub_para_sz, 995);
    encoder.write((const char *)to_pub_para_sz, 995);
    sha256 sh = encoder.result();

    return !!secp256k1_ecdsa_verify(ecc::detail::_get_context(), &sig, (const unsigned char *)sh.data(), &pubkey);
}

bool is_equal_prove_legal(const char* cipher_balance, size_t cb_len, const char* eq_para, size_t eq_len, const char* pub_para, size_t pp_len) {
    if (cb_len < 256 || pp_len < 66 || eq_len < 576) return false;
    if (!cipher_balance || !eq_para || !pub_para) return false;
    if (strlen(cipher_balance) < cb_len || strlen(eq_para) < eq_len || strlen(pub_para) < pp_len) return false;

    unsigned char eq_para_sz[288] = { 0 };
	hex_to_bin(eq_para, 576, eq_para_sz, 288);

    unsigned char pa[33] = { 0 };
    hex_to_bin(pub_para, 66, pa, 33);

    secp256k1_pubkey pubkey;
	memset(&pubkey, 0, sizeof(pubkey));
	if (!secp256k1_ec_pubkey_parse(ecc::detail::_get_context(), &pubkey, pa, 33)) return false;

    unsigned char balance_a[128] = { 0 };
	hex_to_bin(cipher_balance, 256, balance_a, 128);

	return !!secp256k1_elgamal_equal_verify(ecc::detail::_get_context(), &eq_para_sz[128], balance_a, eq_para_sz, &pubkey);
}

bool is_valid_prove_legal(const char* prove_para, size_t pv_len, const char* pub_para, size_t pp_len) {
    if (pv_len < 1990 || pp_len < 66) return false;
    if (!prove_para || !pub_para) return false;
    if (strlen(prove_para) < pv_len || strlen(pub_para) < pp_len) return false;

    unsigned char prove_para_sz[995] = { 0 };
    hex_to_bin(prove_para, 1990, prove_para_sz, 995);

    secp256k1_pedersen_commitment commit;
	secp256k1_bulletproof_string_to_commit(&commit, &prove_para_sz[64], 64);

    if (!secp256k1_bulletproof_rangeproof_verify(ecc::detail::_get_context(), 
        ecc::detail::_get_scratch_space(), 
        ecc::detail::_get_bp_generators(), &prove_para_sz[128], 675, NULL, &commit, 1, 64, &secp256k1_generator_const_h, NULL, 0)) return false;

    unsigned char pa[33] = { 0 };
    hex_to_bin(pub_para, 66, pa, 33);

    secp256k1_pubkey pubkey;
	memset(&pubkey, 0, sizeof(pubkey));
	if (!secp256k1_ec_pubkey_parse(ecc::detail::_get_context(), &pubkey, pa, 33)) return false;

	return !!secp256k1_elgamal_valid_verify(ecc::detail::_get_context(), &prove_para_sz[128+675], prove_para_sz, &pubkey);
}

bool is_cipher_match(const char* random, size_t rand_len, unsigned int value, const char* pub_para, size_t pp_len, const char* ciphertext, size_t ct_len) {
    if (rand_len < 64 || ct_len < 256 || pp_len < 66) return false;
    if (!random || !ciphertext || !pub_para) return false;
    if (strlen(random) < rand_len || strlen(pub_para) < pp_len || strlen(ciphertext) < ct_len) return false;

    unsigned char rand[32] = { 0 };
    hex_to_bin(random, 64, rand, 32);

    unsigned char ct[128] = { 0 };
    hex_to_bin(ciphertext, 256, ct, 128);

    unsigned char pa[33] = { 0 };
    hex_to_bin(pub_para, 66, pa, 33);

    secp256k1_pubkey pubkey;
	memset(&pubkey, 0, sizeof(pubkey));
	if (!secp256k1_ec_pubkey_parse(ecc::detail::_get_context(), &pubkey, pa, 33)) return false;

    unsigned char out[128] = { 0 };
    if (!secp256k1_elgamal_encrypt(ecc::detail::_get_context(), out, &pubkey, rand, value)) return false;

    return memcmp(ct, out, 128) == 0;
}

cipher cipher_add_impl(const char* cipher1, size_t len1, const char* cipher2, size_t len2) {
    if (len1 < 256 || len2 < 256) return cipher();
    if (!cipher1 || !cipher2) return cipher();
    if (strlen(cipher1) < len1 || strlen(cipher2) < len2) return cipher();

    unsigned char c1[128] = { 0 };
    unsigned char c2[128] = { 0 };

    hex_to_bin(cipher1, 256, c1, 128);
    hex_to_bin(cipher2, 256, c2, 128);

    unsigned char c[128] = { 0 };
    if (!secp256k1_elgamal_add(ecc::detail::_get_context(), c, c1, c2)) return cipher();

    return cipher((const char*)c, 128);
}

bool gen_prove_bulletproof( uint64_t value, const unsigned char* blind, size_t b_len, unsigned char* proof, size_t p_len) {
    if (b_len != 32 || p_len != 675) return false;

    return !!secp256k1_bulletproof_rangeproof_prove(ecc::detail::_get_context(), ecc::detail::_get_scratch_space(), ecc::detail::_get_bp_generators(),
        proof, &p_len, nullptr, nullptr, nullptr, &value, nullptr, &blind, nullptr, 1, &secp256k1_generator_const_h, 64, blind, nullptr, nullptr, 0, nullptr);
}

bool gen_pedersen_commit(uint64_t value, const unsigned char *blind, size_t b_len, unsigned char *commit, size_t commit_len) {
    if (b_len != 32 || commit_len != 33) return false;

    secp256k1_pedersen_commitment pds_commit;
    if (!secp256k1_pedersen_commit(ecc::detail::_get_context(), &pds_commit, blind, value, &secp256k1_generator_const_h, &secp256k1_generator_const_g)) {
        return false;
    }

    return !!secp256k1_pedersen_commitment_serialize(ecc::detail::_get_context(), commit, &pds_commit);
}

bool verify_bulletproof(const unsigned char* commit, size_t commit_len, const unsigned char* proof, size_t p_len) {
    if (commit_len != 33) return false;
    if (p_len != 675) return false;

    secp256k1_pedersen_commitment pds_commit;
    if (!secp256k1_pedersen_commitment_parse(ecc::detail::_get_context(), &pds_commit, commit)) return false;

    return !!secp256k1_bulletproof_rangeproof_verify(ecc::detail::_get_context(), ecc::detail::_get_scratch_space(),
        ecc::detail::_get_bp_generators(), proof, p_len, NULL, &pds_commit, 1, 64, &secp256k1_generator_const_h, NULL, 0);
}
}
