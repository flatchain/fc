//
// Created by oc on 2019-08-16.
//

#pragma once

#include <string.h>

namespace fc 
{

class cipher 
{
  public:
    cipher() {
        memset( d, 0, sizeof(d) );
    }
    explicit cipher( const string& hex_str ) {
        auto bytes_written = fc::from_hex( hex_str, (char *)d, sizeof(d) );
      if( bytes_written < sizeof(d) )
         memset( (char*)d + bytes_written, 0, (sizeof(d) - bytes_written) );
    }
    explicit cipher( const char *data, size_t size ) {
        if (size != sizeof(this->d))
	        FC_THROW_EXCEPTION( exception, "cipher: size mismatch" );
        memcpy(this->d, data, size );
    }

    string str()const {
        return fc::to_hex( (char*)d, sizeof(d) );
    }
    operator string()const {
        return  str();
    }

    template<typename T>
    inline friend T& operator<<( T& ds, const cipher& ep ) {
      ds.write( ep.d, sizeof(ep) );
      return ds;
    }

    template<typename T>
    inline friend T& operator>>( T& ds, cipher& ep ) {
      ds.read( ep.d, sizeof(ep) );
      return ds;
    }
    friend cipher operator << ( const cipher& h1, uint32_t i       );
    friend cipher operator >> ( const cipher& h1, uint32_t i       );

    char*    data()const { return (char*)&d[0]; }
    size_t data_size()const { return 128; }

    char d[128];
};

    class variant;
    void to_variant( const cipher& bi, variant& v );
    void from_variant( const variant& v, cipher& bi );

    bool is_ecdsa_signature_legal(const char* eq_para, size_t eq_len, const char* from_pub_para, size_t fp_len, const char* to_pub_para, size_t tp_len, const char* sig, size_t sig_len);
    bool is_equal_prove_legal(const char* cipher_balance, size_t cb_len, const char* eq_para, size_t eq_len, const char* pub_para, size_t pp_len);
    bool is_valid_prove_legal(const char* prove_para, size_t pv_len, const char* pub_para, size_t pp_len);
    bool is_cipher_match(const char* random, size_t rand_len, unsigned int value, const char* pub_para, size_t pp_len, const char* ciphertext, size_t ct_len);
    cipher cipher_add_impl(const char* cipher1, size_t len1, const char* cipher2, size_t len2);

    bool gen_prove_bulletproof( uint64_t value, const unsigned char* blind, size_t b_len, unsigned char* proof, size_t p_len);
    bool verify_bulletproof(const unsigned char* commit, size_t commit_len, const unsigned char* proof, size_t p_len);
    bool gen_pedersen_commit(uint64_t value, const unsigned char *blind, size_t b_len, unsigned char *commit, size_t commit_len);
}

#include <fc/reflect/reflect.hpp>
FC_REFLECT_TYPENAME( fc::cipher )
